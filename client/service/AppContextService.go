package service

import (
	"flag"
	"fmt"
	"gitlab.com/wushyrussia/go_chat/client/model"
	"gitlab.com/wushyrussia/go_chat/client/util"
	"gitlab.com/wushyrussia/go_chat/common/consts"
	"net"
	"os"
)

//The InitInputChanel adds an active input channel to the application context
func InitInputChanel() {
	inputChanel := make(chan string, 1)

	AppContext.InputChanel = inputChanel
}

//The InitConnection starts the connection to the server using the default server address or from the cmd arguments
func InitConnection() {
	serverAddress := flag.String("server", "127.0.0.1:8085", "server address with port")
	flag.Parse()

	conn, connectionError := net.Dial("tcp", *serverAddress)
	if connectionError != nil {
		fmt.Printf("Error at InitConnection: %s", connectionError)
		fmt.Println("Try to restart app with another server address.")
		fmt.Println("Application will closed")
		os.Exit(0)
	}

	AppContext.Connection = &conn
}

//The InitUserName adds username to app context
func InitUserName() {
	fmt.Print("Enter your name: ")

	select {
	case userName := <-AppContext.InputChanel:
		AppContext.UserName = &userName
	}
}

//The InitRoomContext
//*creates chat room context
//*sends info about room joining to server
//*process server response
func InitRoomContext(roomName string) {
	chatChanel := make(chan string, 1)

	AppContext.ActiveChat = model.ChatRoom{
		RoomName:   roomName,
		ChatChanel: chatChanel,
		IsActive:   true}

	util.SendInput(consts.JoinRoom, &AppContext)

	serverData := util.GetServerData(&AppContext)

	printWelcomeRoomMsg(roomName)
	fmt.Println(serverData.ServerData.ActiveChatMsg.Input)

}

func ClearRoomContext() {
	AppContext.ActiveChat = model.ChatRoom{}
}

func CloseApp() {
	util.SendInput(consts.DisconnectClient, &AppContext)

	os.Exit(0)
}

//The UpdateActiveChatList sends a request for an active chat list to the server and processes the response
func UpdateActiveChatList() {
	util.SendInput(consts.GetActiveChatList, &AppContext)

	serverData := util.GetServerData(&AppContext)
	AppContext.ActiveChatList = serverData.ServerData.ServerChatList
}
