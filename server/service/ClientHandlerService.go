package service

import (
	"fmt"
	"gitlab.com/wushyrussia/go_chat/common/consts"
	"gitlab.com/wushyrussia/go_chat/common/dto"
	"net"
)

//The ClientHandler processes connection until it is closed
func ClientHandler(conn net.Conn) {
	defer conn.Close()

	for {
		requestClient, err := GetClient(conn)

		if err != nil {
			connections := RemoveConnection(AppContext.ConnectionList, conn)
			if connections != nil {
				AppContext.ConnectionList = connections
			}
			conn.Close()
			break
		}

		clientData := requestClient.ClientData
		serverData := dto.ServerData{ActiveChatMsg: clientData}
		client := dto.Client{ServerData: serverData}

		if clientData.UserName != "" {
			switch clientData.Input {
			case consts.StopChat:
				RemoveUserFromChatContext(client.ServerData.ActiveChatMsg.ActiveChatName, conn)
			case consts.GetActiveChatList:
				sendChatList(conn)
			case consts.JoinRoom:
				joinRoom(client, &conn)
			case consts.DisconnectClient:
				connections := RemoveConnection(AppContext.ConnectionList, conn)
				if connections != nil {
					AppContext.ConnectionList = connections
				}
				conn.Close()
			default:
				publishMsg(client)
			}
		}
	}
}

//The sendChatList sends list of server chat rooms
func sendChatList(connection net.Conn) {
	serverData := dto.ServerData{ServerChatList: GetChatNameList()}
	clientDto := dto.Client{ServerData: serverData}

	EncodeDtoAndSend(connection, clientDto)
}

//The joinRoom processes the client's connection to the chat and sends service information to this chat
func joinRoom(data dto.Client, conn *net.Conn) {
	chatName := data.ServerData.ActiveChatMsg.ActiveChatName
	userName := data.ServerData.ActiveChatMsg.UserName
	data.ServerData.ActiveChatMsg.Input = fmt.Sprintf("-> %s connected", userName)
	data.ServerData.ActiveChatMsg.UserName = ""

	AddUserToChatContext(chatName, conn)
	publishMsg(data)
}

//The publishMsg sends user msg each chat recipients(current user chat)
func publishMsg(client dto.Client) {
	for _, chat := range AppContext.ChatList {
		if chat.Name == client.ServerData.ActiveChatMsg.ActiveChatName {
			for _, connection := range chat.UserList {
				EncodeDtoAndSend(connection, client)
			}
			break
		}
	}
}
