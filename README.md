# go_chat

This is a client-server application based on Golang, running over tcp protocol

Start the server via the command line or run it in docker container. Also, you can start the server using monk(starts on
default port: 8085 ).

By default, the server has 3 rooms defined on the server: "General chat", "Next gen", "Programmers chat", You can add
new rooms at server startup using command line arguments.

To connect, the client needs to specify the address and port of the server.

## Client

To get the client

```
go get gitlab.com/wushyrussia/go_chat/client
```

Running using the default server URL 'localhost:8085'
```
go run gitlab.com/wushyrussia/go_chat/client
```

Running with custom URL 'some_url_or_ip:8085'
```
go run gitlab.com/wushyrussia/go_chat/client -server="some_url_or_ip:server_port"
```

## Server
- Default port '8085'
- Default chat rooms "General chat",  "Next gen chat",  "Programmers chat"

To get the client
```
go get gitlab.com/wushyrussia/go_chat/server
```

Running using the default server port '8085'
```
go run gitlab.com/wushyrussia/go_chat/server
```

Also, you can start using a custom port:
```
go run gitlab.com/wushyrussia/go_chat/server -port=8080
```

Also, you can add chat rooms
```
go run gitlab.com/wushyrussia/go_chat/server -rooms="room 1,room 2,room 3"
```

### using with docker:
build server docker img:
```
cd $project_folder/server/Deployment
docker build -t go_server_image .
```
run server img
```
docker run go_server_image
```
stop server img
```
docker container ls
docker stop <containerId>
```

### using with Monk:
```
cd $project_folder/server/Deployment/monk
monk load app.yaml
monk run wushyrussia/app
```